#!/bin/zsh -e

# Script that can rsync file(s) or directory(ies) to their corresponding deployed POD.
#
# This allows quick (< seconds!) visualization and f/b for changes _without_ having to do
# a full commit and push (and wait for CI/CD pipeline).
#
# It uses `kubectl` and `rsync` (and not much else! just some glue..) to figure out where to send
# the file and `kubectl exec` ultimately.
#
# You should have a ~/.kube/config file or similar and this should already do something interesting:
#    kubectl get pod
#
# If you happen to have a podspec with multiple containers, it will write to the first container.


mydir=${0:a:h}

function main () {
  source $mydir/aliases # for 'branch2pod'
  setup "$@"
}


function setup() {
  # adjust to wherever `kubectl` and `rsync` live on laptop
  PATH="$PATH:/usr/local/bin"


  if [ "$#" -le 0 ]; then
    echo "

Usage: $ME [source filename or dir] .. [source filename or dir]
  When invoker is \`cd\`-ed somewhere in a git repo (at any branch).
  This is useful to copy 1 or more files to the relevant pod.

NOTE: the pod (and docker image) must have the \`rsync\` package installed.

"
    exit 1
  fi

  sync-to-pod "$@"
}


function sync-to-pod() {
  for FILE_OR_DIR in "$@"; do

    FILE_OR_DIR=$(realpath "$FILE_OR_DIR")

    [ "$FILE_OR_DIR" = ""  -o  ! -e "$FILE_OR_DIR" ]  &&  exit 1 # cautiously exit :-)


    # our repository name is normally (and really _should_ be!) our namespace name
    local DIR=$(dirname "$FILE_OR_DIR")

    # find the branch based on DIR
    [ "$BRANCH" = "" ]  &&  BRANCH=$(git -C "$DIR" rev-parse --abbrev-ref HEAD)

    # now split the FILE name into two pieces -- 'the root of the git tree' and 'the rest'
    local TOP=$(git -C "$DIR" rev-parse --show-toplevel)
    local REST=$(echo "$FILE_OR_DIR" | perl -pe "s|^$TOP||; s|^/+||;")

    # switch dirs so 'branch2pod' works
    [ "$POD" = "" ]  &&  cd "$DIR"  &&  POD=$(branch2pod $BRANCH)  &&  cd -

    for var in FILE_OR_DIR DIR TOP REST BRANCH POD; do
      echo $var="${(P)var}"
    done

    # if pod not found, nothing to do
    [ "$POD" = "" ]  &&  echo 'has this branch run a full pipeline and deployed a Review App yet?'
    [ "$POD" = "" ]  &&  exit 1 # no relevant pod found - nothing to do


    # enough talk!  do it!
    echo
    cd "$TOP"
    set -x
    rsync -PavR --blocking-io --rsync-path="" --rsh="kubectl exec $POD -i -- " "$REST" rsync:"$REST"
    set +x
    cd -
  done
}

ME="$0"
main "$@"
