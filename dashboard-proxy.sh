#!/bin/zsh -ex

# run this on your laptop -- proxies out IA webnode ports and kubernetes dashboard
#
# helpful:
#   https://github.com/kubernetes/dashboard/wiki/Creating-sample-user
#   https://docs.giantswarm.io/guides/install-kubernetes-dashboard/

mydir=${0:a:h}
source $mydir/aliases


function main() {
  set +e
  ssh $(k8-master) 'pkill -f "kubectl proxy"'
  set -e


  echo "
kubernetes dashboard should then be at:
  http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
"

  TOKFI=$HOME/.kube/dashboard-token.txt
  if [ ! -e $TOKFI ]; then
    # create the service account and secret
    DSA=cluster-admin-dashboard-sa
    kubectl create serviceaccount $DSA
    kubectl create clusterrolebinding $DSA --clusterrole=cluster-admin --serviceaccount=default:$DSA

    # get the created token for the secret
    SECRET_NAME=$(kubectl get secrets -o=custom-columns=NAME:.metadata.name |fgrep $DSA-token |head -1)
    kubectl describe secret $SECRET_NAME |egrep ^token: |rev |cut -f1 -d' '|rev > $TOKFI

    # put it into our normal .kube/config default file -- use this for login auth below
    kubectl config set-credentials kubernetes-admin --token="$(cat $TOKFI)"
  fi

  set -x
  ssh -L 8001:localhost:8001 $(k8-master) "kubectl proxy --accept-hosts='^*$'"
  set +x
}



main
