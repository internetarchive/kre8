# kre8 -- Create Kubernetes => Create K8 => Kre8

Create a Kubernetes cluster and setup full GitLab Auto Dev Ops pipelines "on premise" (using 1+ ssh-able unix VMs) for full CI/CD of GitLab repositories
(GitLab can be self-hosted "on premise" as well, optional)

![screenshot](pipeline.png)
*example full pipeline*

## Requirements for Creating your Kubernetes cluster:
- One or more **linux** nodes you can `ssh` to and have `sudo` privs
- mac (or make minor adjustments below)
  - `kubectl` and `helm` on computer, eg: `brew install kubernetes-cli kubernetes-helm`


## Overview
- Uses _blisteringly fast_ [k3s](https://k3s.io/) to setup kubernetes cluster
- [lets encrypt](https://letsencrypt.org/) automatic `https` is built-in to [traefik](https://containo.us/traefik/) - and has minor issues we sort out
- GitLab std. integration gets minor workarounds to create ingresses in k3s-based clusters
- NOTE: Persistent Volumes will get allocated automatically as local file storage
  and live here: `/var/lib/rancher/k3s/storage`


---
## Create a K8 cluster as needed

### Setup cluster first/leader node
- set env var `LEADER=[your-leader-FQDN-hostname]` and run:
```bash
# if you use ferm for firewalling ports, use this or alter it to unblock required TCP/UDP ports
wget -qO- https://gitlab.com/internetarchive/kre8/-/raw/master/ports-unblock.sh | ssh ${LEADER?} bash -ex

# to ensure we get modern traefik v2+ _and_ get https automatically working, defer traefik install
ssh ${LEADER?} "curl -sLS https://get.k3s.io | INSTALL_K3S_EXEC='server --disable traefik' sh -"
KUBECONFIG=$HOME/.kube/config
rm -fv $KUBECONFIG
ssh ${LEADER?} sudo cat /etc/rancher/k3s/k3s.yaml |perl -pe 's/127.0.0.1:6443/'$LEADER':6443/' |tee $KUBECONFIG
chmod 400 $KUBECONFIG

echo 'now wait a bit for calls like this to show READY 1/1 everywhere, etc. then CTL-C and proceed below'
kubectl get -A pod -w
```


### Install newer traefik _with_ ACME enabled
```bash
LEADER=$(fgrep server ~/.kube/config |cut -f3 -d:|tr -d /)
# NOTE: guessing a bit here for your admin email address.  adjust if/as needed.
DOMAIN=$(echo $LEADER |rev |cut -f1-2 -d .|rev)
ACME_EMAIL=${USER?}@${DOMAIN?}

# The default google helm chart uses deprecated v1.7.  Per Containous maintainers - use v2+
helm repo add traefik  https://helm.traefik.io/traefik
helm repo update


# Use letsencrypt production (not staging) server with tlsChallenge.
# If you want to see/debug ACME progress, toggle the `log.level` setting below.
echo '
additionalArguments:
  - "--certificatesresolvers.letsencrypt.acme.email='${ACME_EMAIL?}'"
  - "--certificatesResolvers.letsencrypt.acme.tlsChallenge=true"
  - "--certificatesresolvers.letsencrypt.acme.storage=/data/acme.json"
  - "--serversTransport.insecureSkipVerify=true"
  - "--global.sendAnonymousUsage=false"
  - "--accesslog=true"
# - "--log.level=DEBUG"
  #
  # this does global http => https 301 redirects for your deployment urls
  - "--entrypoints.web.http.redirections.entrypoint.scheme=https"
  - "--entrypoints.web.http.redirections.entrypoint.to=:443"
  - "--entrypoints.web.address=:8000"
' >| traefik-values.yaml


helm upgrade --install --namespace kube-system traefik traefik/traefik -f traefik-values.yaml
rm -f traefik-values.yaml
helm ls -A

# inspect source if desired
#   helm pull --untar traefik/traefik

echo 'now wait a bit for calls like this to show READY 1/1 everywhere, etc. then CTL-C and proceed below'
kubectl get -A pod -w
```


### Add additional nodes to cluster (optional)
- set AGENT=[additional-hostname-for-cluster]
```bash
LEADER=$(fgrep server ~/.kube/config |cut -f3 -d:|tr -d /)
TOKEN=$(ssh ${LEADER?} sudo cat /var/lib/rancher/k3s/server/node-token)
wget -qO- https://gitlab.com/internetarchive/kre8/-/raw/master/ports-unblock.sh | ssh ${AGENT?} bash -ex
ssh ${AGENT?} "curl -sfL https://get.k3s.io | K3S_URL=https://${LEADER?}:6443 K3S_TOKEN=${TOKEN?} sh -"
```

---
## Setup GitLab Groups and Projects
- Setup a _group-level_ integration with your K8 cluster - go your group page, eg: https://gitlab.com/internetarchive
- Find the left-hand nav and the `[Kubernetes]` section
- Press the `[Add Kubernetes cluster]` button
- Hit the `[Add existing cluster]` tab

### Kubernetes cluster name:
enter something unique (doesn't matter too much)

### Three type-ins:
Find  url;  certificate text (including [BEGIN .. END] lines);  long final token string from:
[add-existing-cluster-settings](add-existing-cluster-settings)

### RBAC-enabled cluster:
make sure this _checkbox_ is checked

### GitLab-managed cluster:
uncheck this checkbox


### for each repo/project in the group you deploy
paste this into top-level dir `.gitlab-ci.yml`
```yaml
include:
  - remote: 'https://gitlab.com/internetarchive/kre8/-/raw/master/k3s-ci.yml'
```


---
## Admin notes
### uninstall kubernetes cluster
- ssh to each node in your cluster and run:
`k3s-uninstall.sh`


### reinstalling a node
- if you remove a node from cluster (eg: reinstall from scratch), you'll need to edit on LEADER:
  - `/var/lib/rancher/k3s/server/cred/node-passwd`
  - before that same node can re-join the cluster


---
# Setup optional Applications
- If you _want_ to use GitLab runner in your k8 cluster for CI/CD:
  - Find the group's `[Kubernetes]` button on the left menu and find the cluster
  - Click the [Applications] tab (not super prominent - it's there..)
  - Do the `Helm Tiller` `[Install]` setup _first_
  - Do `GitLab Runner` button `[Install]` next
  - Suggest make it a "group runner" - go to [Settings] [CI/CD] [Runners] then [Enable group Runners]


---
# Sample fast "hi world" webapp to see a full pipeline running
- Add to top dir of a gitlab repo a file named `.gitlab-ci.yml` ([custom CI/CD](k3s-ci.yml) that plays nice with `k3s`'s `traefik` loadbalancer) with contents:
```yaml
include:
  - remote: 'https://gitlab.com/internetarchive/kre8/-/raw/master/k3s-ci.yml'

# optional
test:
  stage: test
  needs: ["build"]
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}:${CI_COMMIT_SHA}
  script:
    - cd /tmp
    - echo 'change this to run some real tests..'
```
- Add to top dir of repo, file `Dockerfile`, contents:
```bash
FROM node:alpine
USER node
CMD node --input-type=module -e "import http from 'http'; http.createServer((req, res) => res.end('hai\n')).listen(5000)"
```
- NOTE: by default, kubernetes assumes your webapp serves http (not https) on port `5000`.  You can override, but it's easier to adjust your webapp port to `5000` (like above `Dockerfile`).  K8 will automatically healthcheck for a 200 response on port 5000 (and restart your container after X seconds of nonresponses -- assuming it has gone AWOL).


---
## Pipelines and "Review App" URLs
- Find your project in GitLab; in sidebar, go to [CI/CD] [Pipelines]
- find the pipeline from your commit/push and the [review] or [production] log will show your website url
- Otherwise, @see [aliases](aliases) and the `kapp` alias


## TODO future
- update `traefik` to automatically insert HSTS header line in https replies


## Kubernetes Overview
- high level intro _excellent_ primer diagram and overview of concepts, history, rationale:
- https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/
- https://coreos.com/kubernetes/docs/latest/services.html
- FYI: google states that Kubernetes supports clusters up to 100 nodes with 30 pods per node, with 1–2 containers per pod.  Typical Docker use case are 5-10+ containers/host.


## Kubernetes - moar Documention:
- https://k3s.io/
- https://github.com/rancher/k3s
- https://kubernetes.io/docs/user-guide/kubectl-cheatsheet/
- https://kubernetes.io/docs/tutorials/stateless-application/run-stateless-application-deployment/
- https://blog.giantswarm.io/getting-started-with-a-local-kubernetes-environment/


## Helpful traefik links:
- https://docs.traefik.io/v2.2/reference/static-configuration/file/
- https://docs.traefik.io/v2.2/https/acme
- https://medium.com/@yanick.witschi/automated-kubernetes-deployments-with-gitlab-helm-and-traefik-4e54bec47dcf
- https://medium.com/dev-genius/quickstart-with-traefik-v2-on-kubernetes-e6dff0d65216
- https://docs.traefik.io/migration/v1-to-v2/#http-to-https-redirection-is-now-configured-on-routers
- https://docs.traefik.io/middlewares/overview/ (bleah)
- https://docs.traefik.io/middlewares/headers/#using-security-headers


## Other Reading
### Possible way to setup multiple 'services' in same build/deploy..
- https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#how-to-use-other-images-as-services
